int arr[] = {3, 5, 6, 9, 10, 11, 12, 13};

void setup() {
  for (byte i = 3; i <= 7; ++i) {
    pinMode(i, OUTPUT);
  }
  Serial.begin(9600);
}

void loop() {
  static byte leds = 3;
  static int steps = 1;
  Serial.println(leds);
  digitalWrite(leds, !digitalRead(leds));
  leds += steps;
  if (leds <= 3 || leds >= 8) {
    steps = -steps;
  }
  delay(600);
}
