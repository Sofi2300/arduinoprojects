void setup() {
  pinMode(3, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(13, OUTPUT);
}

void loop() {
  int arr[] = {3, 5, 6, 9, 10, 11, 12, 13};
  int i = 0; 
  while (i <= 6) {
    digitalWrite (arr[i], LOW);
    delay(100);
    digitalWrite (arr[i], HIGH);
    delay(100);
    ++i;
  }
  int b = 7;
  while (b >= 0) {
    digitalWrite (arr[b], LOW);
    delay(100);
    digitalWrite (arr[b], HIGH);
    delay(100);
    --b;
  }
}
