#include <IRremote.h>
const int IR_PIN = 2;
IRrecv receiver(IR_PIN);
decode_results results;
void setup() {
  Serial.begin(9600);
  receiver.enableIRIn();
}

void loop() {
  if (receiver.decode(&results)) {
    Serial.println(results.value, HEX);
    receiver.resume();
  }
}
