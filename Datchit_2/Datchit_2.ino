//программа управления зуммером, при помощи инфракрасного датчика
//программа управления светодиода, при помощи инфракрасного датчика
#include <IRremote.h>
const int BUZZER = 0xFF02FD;
const int R = 0xFF6897;
const int G = 0xFF9867;
const int B = 0xFFB04F;
const int BG = 0xFF18E7;
const int IR_PIN = 2;
IRrecv receiver(IR_PIN);
decode_results results;
void setup() {
  Serial.begin(9600);
  receiver.enableIRIn();
  pinMode(9, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
}

void loop() {
  while (receiver.decode(&results)) {
    Serial.println(results.value, HEX);
    receiver.resume();
    switch(results.value){
      case 0xFF6897: // - R
        digitalWrite(6, HIGH);
        break;
      case 0xFF9867:
        digitalWrite(10, HIGH);
        break;
      case 0xFFB04F:
        digitalWrite(11, HIGH);
        break;
      case 0xFF18E7:
        digitalWrite(11, HIGH);
        digitalWrite(10, HIGH);
        break;
      default:
        digitalWrite(6, LOW);
        digitalWrite(10, LOW);
        digitalWrite(11, LOW);
        break;
      }
  }
}
