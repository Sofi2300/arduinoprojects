#include "Wire.h" 
#include "LiquidCrystal_I2C.h"
 

uint8_t bukva_Ya[8] = {
  B01110,
  B11111,
  B01010,
  B10001,
  B01110,
  B00000,
  B00000
};

 
LiquidCrystal_I2C lcd(0x27,16,2);  // устанавливаем адрес 0x27, и дисплей 16 символов в 2 строки (16х2)
 
void setup()
{
  lcd.init();                      // инициализация LCD 
  lcd.backlight();                 // включаем подсветку
  lcd.clear();                     // очистка дисплея

  lcd.createChar(1, bukva_Ya);
}
 
void loop(){
  lcd.setCursor(0, 0);
  lcd.write(1);
  
  for (int i = 0; i<=14; ++i){
      lcd.scrollDisplayRight();
      delay(200);
  }
  
  for (int a = 15; a>=1; --a){
    lcd.scrollDisplayLeft();
    delay(200);
  }

}
