//программа управления зуммером, при помощи инфракрасного датчика
//программа управления светодиода, при помощи инфракрасного датчика
#include <IRremote.h>
const int BUZZER = 0xFF02FD;
const int R = 0xFF6897;
const int G = 0xFF9867;
const int B = 0xFFB04F;
const int BG = 0xFF18E7;
const int IR_PIN = 2;
IRrecv receiver(IR_PIN);
decode_results results;
void setup() {
  Serial.begin(9600);
  receiver.enableIRIn();
  pinMode(9, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
}

void loop() {
  while (receiver.decode(&results)) {
    Serial.println(results.value, HEX);
    receiver.resume();
    if (results.value == 0xFF02FD) {
      digitalWrite(9, HIGH);
      Serial.println(BUZZER, "on");
    }
    else {
      digitalWrite(9, LOW);
      Serial.println(BUZZER, "off");
    }
    if (results.value == 0xFF6897) {
      digitalWrite(6, HIGH);
      Serial.println(R, "on");
    }
    else {
      digitalWrite(6, LOW);
      Serial.println(R, "off");
    }
    if (results.value == 0xFF9867) {
      digitalWrite(10, HIGH);
      Serial.println(G, "on");
    }
    else {
      digitalWrite(10, LOW);
      Serial.println(G, "off");
    }
    if (results.value == 0xFFB04F) {
      digitalWrite(11, HIGH);
      Serial.println(B, "on");
    }
    else {
      digitalWrite(11, LOW);
      Serial.println(B, "off");
    }
    if (results.value == 0xFF18E7) {
      digitalWrite(10, HIGH);
      digitalWrite(11, HIGH);
      Serial.println(BG, "on");
    }
    else {
      digitalWrite(10, LOW);
      digitalWrite(11, LOW);
      Serial.println(BG, "off");
    }
  }
}
