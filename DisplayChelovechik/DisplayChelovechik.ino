#include "Wire.h" 
#include "LiquidCrystal_I2C.h"
 

uint8_t Chelovechik[8] = {
  B00100,
  B01110,
  B10001,
  B01110,
  B11111,
  B01010,
  B01010
};

 
LiquidCrystal_I2C lcd(0x27, 16, 2);  // устанавливаем адрес 0x27, и дисплей 16 символов в 2 строки (16х2)
 
void setup(){
  Serial.begin(9600);
  lcd.init();                      // инициализация LCD 
  lcd.backlight();                 // включаем подсветку
  lcd.clear();                     // очистка дисплея

  lcd.createChar(1, Chelovechik);
}
 
void loop(){

  int fi = digitalRead(3), s = digitalRead(4);
  lcd.setCursor(0, 0);
  lcd.write(1);

  if(fi){
    lcd.scrollDisplayRight();
  }
  else if(s){
    lcd.scrollDisplayLeft();
  }

}
