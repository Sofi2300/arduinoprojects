// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// Счётчик с кратным нажатию приращением, ограничителем по количеству приращений и простым "антидребегом"
// V 1.0
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
#include <Arduino.h>

void setup() {
  Serial.begin(9600);
}

void loop() {
  static int count = 0;
  static bool currient = false;
  static bool previous = false;
  bool buttonState = digitalRead(3);
  while (previous == true && buttonState == false) {
    while (++count) {
      currient = !currient;
      //Serial.println(currient); // Состояние флага кнопки
      //Serial.println(count); // Счётчик
      if (count >= 6) { // Ограничение счётчика
        count = 0;
      }
      switch (count) {
        case 1:
          Serial.println("Выбран 1");
          analogWrite(11, 255);
          analogWrite(9, 0);
          analogWrite(10, 0);
        break;
        case 2:
          Serial.println("Выбран 2");
          analogWrite(11, 0);
          analogWrite(10, 255);
          analogWrite(9, 0);
        break;
        case 3:
          Serial.println("Выбран 3");
          analogWrite(10, 0);
          analogWrite(9, 255);
          analogWrite(11, 0);
        break;
        case 4:
          Serial.println("Выбран 4");
          analogWrite(9, 255);
          analogWrite(10, 255);
          analogWrite(11, 255);
        break;
        case 5:
          Serial.println("Выбран 5");
          analogWrite(10, 0);
          analogWrite(11, 0);
          analogWrite(9, 0);
        break;  
        default:
        break;
      }
      break;
    }
    break;
  }
  previous = buttonState;
}
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
// END FILE
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- //
