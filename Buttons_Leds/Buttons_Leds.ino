void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(2, INPUT_PULLUP);
  pinMode(3, INPUT_PULLUP);
  pinMode(4, INPUT_PULLUP);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(13, OUTPUT);
  
}

void loop() {
  // put your main code here, to run repeatedly:
  bool buttonOne = digitalRead(2);
  bool buttonTwo = digitalRead(3);
  bool buttonThree = digitalRead(4);
  digitalWrite(7, !buttonOne);
  digitalWrite(8, !buttonTwo);
  digitalWrite(13, !buttonThree);
  Serial.print(buttonOne);
  Serial.print(buttonTwo);
  Serial.println(buttonThree);
}
