const int arr[] = {3, 5, 6, 9, 10, 11, 12, 13}; // Одномерный массив с пинами

void setup() {
  // Настраиваем все светодиоды в режим управления
  for (byte i = 0; i <= 7; ++i) {
    
    //Обьявляем пины светодиодов в массиве
    pinMode(arr[i], OUTPUT);
  }
  // Выключаем все светодиоды
  for (byte i = 0; i <= 7; ++i) {
    digitalWrite(arr[i], !LOW);
  }
  Serial.begin(9600); // Для диагностики
}

void loop() {
  // Переменная пина светодиода
  static byte leds = 0;
  // Переменная с каким шагом перемещать светодиоды
  static int steps = 1;
  Serial.println(leds); // Диагностика  
  // 
  //Шаг светодиода
  leds += steps;
  digitalWrite(arr[leds], !HIGH);// Включаем светодиоды
  delay(400);// Ждём
  digitalWrite(arr[leds], !LOW);// Выключаем светодиоды
  delay(400);// Ждём
  if (leds <= 0 || leds >= 7) {
    // Светодиоды идут в обратную сторону
    steps = -steps;
  }
}
