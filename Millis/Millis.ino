void setup() {
  Serial.begin(9600);
}

void loop() {
    auto times = millis() / 1000;
    int Sec = (times % 3600) % 60;
    int Minuts = (times % 3600) / 60;
    int Hours = (times / 3600); 
    Serial.print(":");
    Serial.print(Minuts);
    Serial.print(":");
    Serial.println(Sec);
    Serial.print(Hours);
}
