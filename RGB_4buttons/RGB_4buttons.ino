void setup() {
  pinMode (9, OUTPUT);
  pinMode (11, OUTPUT);
  pinMode (10, OUTPUT);
}

void loop() {
  bool button1 = digitalRead(3);
  bool button2 = digitalRead(4);
  bool button3 = digitalRead(5);
  bool button4 = digitalRead(6);
  if (button1 != false) {
    digitalWrite(9, HIGH);
  }
  else {
    digitalWrite(9, LOW);
  }
  if (button2 != false) {
    digitalWrite(10, HIGH);
  }
  else {
    digitalWrite(10, LOW);
  }
  if (button3 != false) {
    digitalWrite(11, HIGH);
  }
  else {
    digitalWrite(11, LOW);
  }
  if (button4 != false) {
    digitalWrite(9, HIGH);
    digitalWrite(10, HIGH);
    digitalWrite(11, HIGH);
  }
  else {
    digitalWrite(9, LOW);
    digitalWrite(10, LOW);
    digitalWrite(11, LOW);
  }
  

}
