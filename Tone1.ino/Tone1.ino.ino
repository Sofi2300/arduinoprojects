/*

  Melody

  Plays a melody

  circuit:

  - 8 ohm speaker on digital pin 8

  created 21 Jan 2010

  modified 30 Aug 2011

  by Tom Igoe

  This example code is in the public domain.

  http://www.arduino.cchttps://www.arduino.cc/en/Tutorial/Tone

*/
#include "pitches.h"

void setup() {
    tone(9, NOTE_C1, 500); 
    noTone(9);
    tone(9, NOTE_D1, 200);
    noTone(9);
    tone(9, NOTE_E1, 500);
    noTone(9);
    tone(9, NOTE_F1, 200); 
    noTone(9);
    tone(9, NOTE_G1, 500); 

    

    // stop the tone playing:

    //noTone(9);

  }

void loop() {
  tone(9, NOTE_C1, 500); 
    noTone(9);
    tone(9, NOTE_D1, 200);
    noTone(9);
    tone(9, NOTE_E1, 500);
    noTone(9);
    tone(9, NOTE_F1, 200); 
    noTone(9);
    tone(9, NOTE_G1, 500); 


  // no need to repeat the melody.
}
