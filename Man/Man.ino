#include "Wire.h"
#include "LiquidCrystal_I2C.h"


uint8_t manR[8] = {
  B01100,
  B01100,
  B00000,
  B01110,
  B11100,
  B01100,
  B11010,
  B10011
};

uint8_t manL[8] = {
  B00110,
  B00110,
  B00000,
  B01110,
  B11100,
  B01100,
  B01011,
  B11001
  };

uint8_t Stop[8] = {
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111
};

LiquidCrystal_I2C lcd(0x27, 16, 2);  // устанавливаем адрес 0x27, и дисплей 16 символов в 2 строки (16х2)

void setup() {
  Serial.begin(9600);
  lcd.init();                      // инициализация LCD
  lcd.backlight();                 // включаем подсветку
  lcd.clear();                     // очистка дисплея

  lcd.createChar(1, manR);
  //lcd.createChar(2, ChelovechikSt);
  lcd.createChar(3, manL);
  lcd.createChar(8, Stop);
}


void loop() {
  int goLeft = digitalRead(3), goRight = digitalRead(6);

  lcd.setCursor(0, 0);
  lcd.write(1);
  lcd.setCursor(8, 0);
  lcd.write(8);

  if(goRight == 1){
   Serial.println("Right");
   lcd.scrollDisplayRight();
    delay(92);
 }
  if(goLeft == 1){
    Serial.println("Left");
    lcd.scrollDisplayLeft();
    delay(92);
  }
  
  /*  for (int i = 0; i <= 16; ++i) {
    lcd.scrollDisplayRight();
    delay(200);
  }
  
  for (int a = 15; a >= 0; --a) {
    lcd.scrollDisplayLeft();
    delay(200);
  }
 */ 

}
