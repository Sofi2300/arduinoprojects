const int PINS[3] = {7, 8, 13}; // 0 = 7, 1 = 8, 2 = 13

void setup() {
  for (int i = 0; i <= 2; ++i) { // ++i тоже самое, что i = i + 1
    pinMode(PINS[i], OUTPUT);
  }
}

void loop() {
  int RAND = random(0, 3);
  digitalWrite(PINS[RAND], !digitalRead(PINS[RAND]));
  delay(150);
}
