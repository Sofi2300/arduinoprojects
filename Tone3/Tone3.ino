#include "pitches.h"
int durations[] = {
  4, 8, 8, 8, 8, 8, 8, 8, 8, 4, 4, 8, 8, 4, 4, 2
};
int melody[] = {
  NOTE_E4, NOTE_A4, NOTE_GS4, NOTE_A4, NOTE_B4, NOTE_C5, NOTE_D5, NOTE_E5, NOTE_A5, NOTE_E5, NOTE_D5, NOTE_C5, NOTE_E5, NOTE_C5, NOTE_B4, NOTE_A4
};

int durationsA[] = {
  4, 8, 8, 8, 8, 8, 8, 8, 8, 4, 4, 8, 8, 4, 4, 2
};
int melodyA[] = {

};


void setup() {
  Serial.begin(9600);
  int size = sizeof(melody) / sizeof(int);
  for (int i = 0; i < size; i++) {
    int duration = 1000 / durations[i];
    tone(9, melody[i], duration);
    delay(duration);
  }
  noTone(9);
}



void loop() {
  bool buttonState = digitalRead(3);
  Serial.println(buttonState);
}
