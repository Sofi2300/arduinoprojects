const byte RED = 9;
const byte GREEN = 10;
const byte BLUE = 11;
const byte KNOB = A0;
int a = 0;

void setup() {
  pinMode(RED, OUTPUT);
  pinMode(GREEN, OUTPUT);
  pinMode(BLUE, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  int i =  map(analogRead(KNOB), 0, 1024, 0, 767);
  

  if (i <= 255) {
    analogWrite(RED, 255 - i);
    analogWrite(GREEN, i);
    //Serial.println(i); // Для диагностики
   
  } else if (i >= 255) {
    a = i - 255;
    analogWrite(GREEN, 512 - a);
    analogWrite(BLUE, a);
    //Serial.println(i);
  } else if (i >= 512) {
    a = i - 512;
    analogWrite(BLUE, 767 - a);
    analogWrite(RED, a);
    //Serial.println(i);
  }
}
/*
  for (int i = var; i <= var; i += 5) {
  analogWrite(GREEN, i);
  analogWrite(BLUE, 255 - i);
  }
  for (int i = var; i <= var; i += 5) {
  analogWrite(BLUE, i);
  analogWrite(RED, 255 - i);
  }*/
