const byte RED = 9;
const byte GREEN = 10;
const byte BLUE = 11;
const byte KNOB = A0;

void setup() {
  Serial.begin(9600);
  pinMode(RED, OUTPUT);
  pinMode(GREEN, OUTPUT);
  pinMode(BLUE, OUTPUT);
}

void loop() {
  int var =  map(analogRead(KNOB), 0, 1024, 0, 255);
  Serial.println(var);

  for (int i = var; i <= var; i += 5) {
    analogWrite(GREEN, i);
    analogWrite(RED, 255 - i);
    //analogWrite(BLUE, i);
  }
  /*
  for (int i = var; i <= var; i += 5) {
    analogWrite(GREEN, i);
    analogWrite(BLUE, 255 - i);
  }
  for (int i = var; i <= var; i += 5) {
    analogWrite(BLUE, i);
    analogWrite(RED, 255 - i);
  }*/
}
